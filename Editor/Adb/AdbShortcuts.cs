﻿#if UNITY_ANDROID && UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Patik.EditorTools.Android.Shared;
using UnityEditor;
using UnityEditor.Android;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Ping = System.Net.NetworkInformation.Ping;

namespace Patik.EditorTools.Android.Adb
{
    public static class AdbShortcuts
    {
        public static readonly string AdbPath = ADB.GetInstance().GetADBPath();

        [MenuItem("📱 ADB/▶️ Play", priority = Int32.MinValue)]
        public static async void RunApplicationOnMobile()
        {
            if (!await AssertAdbIsReady()) return;

            if (EnsureApplicationCanRun())
            {
                if (IsInTCPIPMode)
                {
                    ClearAdb();
                    WaitForPortAndPrint();
                }
                
                RunAdbCommand($"shell monkey -p {Application.identifier} 1", logErrorsInConsole:false);
            }
        }

        [MenuItem("📱 ADB/⏹️ Stop", priority = Int32.MinValue + 1)]
        public static async void StopApplicationOnMobile()
        {
            if (!await AssertAdbIsReady()) return;
            RunAdbCommand($"shell am force-stop {Application.identifier}");
        }

        [MenuItem("📱 ADB/🔄️ Restart", priority = Int32.MinValue + 2)]
        public static async void RestartApplicationOnMobile()
        {
            if (!await AssertAdbIsReady()) return;

            StopApplicationOnMobile();
            RunApplicationOnMobile();
        }

        [MenuItem("📱 ADB/✔   Install Application", priority = Int32.MinValue + 201)]
        public static async void InstallApplicationToMobile()
        {
            if (!await AssertAdbIsReady()) return;

            var path = EditorUtility.OpenFilePanel("Install Apk ", "", "apk");
            if (path.Length != 0)
            {
                ClearAdb();
                RunAdbCommandAsync($"install {path}", true);
            }
        }

        [MenuItem("📱 ADB/🗑️   Uninstall Application", priority = Int32.MinValue + 202)]
        public static async void UninstallApplicationFromMobile()
        {
            if (!await AssertAdbIsReady()) return;

            RunAdbCommandAsync($"uninstall {Application.identifier}", true);
        }

        [MenuItem("📱 ADB/🛀   Clear Cache and Restart 🔄️", priority = Int32.MinValue + 101)]
        public static async void ClearCacheAndRestart()
        {
            if (!await AssertAdbIsReady()) return;

            StopApplicationOnMobile();
            ClearCache();
            RunApplicationOnMobile();
        }

        [MenuItem("📱 ADB/🛀   Clear Cache", priority = Int32.MinValue + 102)]
        public static async void ClearCache()
        {
            if (!await AssertAdbIsReady()) return;

            var errors = ReadAdbOutput($"-s {AdbDiscovery.SerialNumberOfFirstAttachedDevice()} shell pm clear {Application.identifier}").Error;
            foreach (var line in errors)
            {
                if (line.Contains("does not have permission android.permission.CLEAR_APP_USER_DATA"))
                {
                    EditorUtility.DisplayDialog("Adb Clear", "Cant Clear Cache!!! \n\nMake Sure \"Disable Permission Monitoring\" is set to true in Developer Settings", "💖");
                    return;
                }
            }
        }

        [MenuItem("📱 ADB /📶  Restart and Connect to TCP", priority = Int32.MinValue + 302)]
        public static async void ConnectAdbToTCP()
        {
            await AssertAdbExist();
            AdbDiscovery.Start();
        }

        [MenuItem("📱 ADB /📱  Connected Devices", priority = Int32.MinValue + 401)]
        public static async void ShowConnectedDevices()
        {
            await AssertAdbExist();

            StringBuilder sb = new StringBuilder();
            foreach (var line in ProcessCommunicator.ReadProcessOutput(AdbPath, "devices").Output)
            {
                sb.AppendLine();
                sb.AppendLine(line);
            }

            EditorUtility.DisplayDialog("Adb Connected Devices", sb.ToString(), "💖");
        }

        [MenuItem("📱 ADB /🔌  Last Known Debugger Port", priority = Int32.MinValue + 402)]
        public static async void TellMeLastDebuggerPort()
        {
            if (!await AssertAdbIsReady()) return;

            SearchForLastPort();
        }
        
        [MenuItem("📱 ADB /🐞  Troubleshoot/☠  Kill Server️", priority = Int32.MinValue + 404)]
        public static async void KillServer()
        {
            RunAdbCommand($"kill-server" , sendToFirstAvailableDevice:false);
        }

        private static void ClearAdb() { Process.Start(AdbPath, "logcat -c")?.WaitForExit(); }

        private static void SearchForLastPort()
        {
            SynchronizationContext editorThread = SynchronizationContext.Current;

            var result = string.Empty;

            void OutPutHandler(object sender, DataReceivedEventArgs args)
            {
                if (args.Data.Contains("managed debugger"))
                {
                    result = args.Data;
                }
            }

            void OnExit(object sender, EventArgs args)
            {
                if (string.IsNullOrEmpty(result))
                {
                    editorThread.Post(_ => EditorUtility.DisplayDialog("C# Debug Port", "Can't Find Port, Make Sure Wi-Fi Adb Debugging is Enabled and Application Development Build", "Ok"), null);
                }

                else
                {
                    editorThread.Post(_ => EditorUtility.DisplayDialog("C# Debug Port", result.Substring(result.IndexOf("port") + 4), "Ok"), null);
                }
            }

            RunAdbCommand("logcat -d find | \"managed debugger\"", onDataOutput: OutPutHandler, onExit: OnExit);
        }

        private static async void WaitForPortAndPrint()
        {
            SynchronizationContext editorThread = SynchronizationContext.Current;

            var foundPort = false;

            void OutputHandler(object sender, DataReceivedEventArgs args)
            {
                if (args.Data.Contains("managed debugger"))
                {
                    var port = args.Data.Substring(args.Data.IndexOf("port") + 4).Trim();
                    editorThread.Post(_ =>
                    {
                        if (EditorUtility.DisplayDialog("C# Debug Port", port, "Copy To Clipboard", "Ok"))
                        {
                            EditorGUIUtility.systemCopyBuffer = port;
                        }
                    }, null);

                    foundPort = true;
                    var senderProcess = (Process) sender;
                    senderProcess.Kill();
                }
            }

            await RunAdbCommandAsync("logcat find | \"managed debugger\"", onDataOutput: OutputHandler, timeoutInMilliseconds: 20000);

            if (!foundPort)
            {
                EditorUtility.DisplayDialog("C# Debug Port", "Can't Find Port, Make Sure Wi-Fi Adb Debugging is Enabled", "Ok");
            }
        }

        public static List <string>                               ReadAdbOutputLines(string command) { return ProcessCommunicator.ReadProcessOutputLines(AdbPath, command); }
        public static (List <string> Output, List <string> Error) ReadAdbOutput(string command) { return ProcessCommunicator.ReadProcessOutput(AdbPath, command); }

        private static Process RunAdbCommand(string                   command,
                                             bool                     logDataInConsole           = false,
                                             bool                     logErrorsInConsole         = true,
                                             DataReceivedEventHandler onDataOutput               = null,
                                             DataReceivedEventHandler onErrorOutput              = null,
                                             EventHandler             onExit                     = null,
                                             bool                     sendToFirstAvailableDevice = true)
        {
            if (sendToFirstAvailableDevice) command = $"-s {AdbDiscovery.SerialNumberOfFirstAttachedDevice()} {command}";
            return ProcessCommunicator.RunCommand(AdbPath, command, logDataInConsole, logErrorsInConsole, onDataOutput, onErrorOutput, onExit);
        }

        private static Task RunAdbCommandAsync(string                   command,
                                               bool                     logDataInConsole           = false,
                                               bool                     logErrorsInConsole         = true,
                                               DataReceivedEventHandler onDataOutput               = null,
                                               DataReceivedEventHandler onErrorOutput              = null,
                                               EventHandler             onExit                     = null,
                                               int                      timeoutInMilliseconds      = -1,
                                               bool                     sendToFirstAvailableDevice = true)
        {
            if (sendToFirstAvailableDevice) command = $"-s {AdbDiscovery.SerialNumberOfFirstAttachedDevice()} {command}";
            return ProcessCommunicator.RunCommandAsync(AdbPath, command, logDataInConsole, logErrorsInConsole, onDataOutput, onErrorOutput, onExit, timeoutInMilliseconds);
        }

        private static List <string> GetConnectedDevices()
        {
            var adbOutput = ProcessCommunicator.ReadProcessOutput(AdbPath, "devices").Output;
            //First Line is "Attached Devices" echo , remove it
            adbOutput.RemoveAt(0);
            for (var i = 0; i < adbOutput.Count; i++)
            {
                adbOutput[i] = adbOutput[i].Split('\t')[0];
            }

            return adbOutput;
        }

        private static async Task AssertAdbExist()
        {
            var adbDaemonStarted = Process.GetProcessesByName("adb").Length != 0;
            if (!adbDaemonStarted)
            {
                var timeout          = 10000;
                var unityContext     = SynchronizationContext.Current;
                var command          = RunAdbCommandAsync("start-server", timeoutInMilliseconds: timeout , sendToFirstAvailableDevice:false);
                var commandStartTime = DateTime.Now;

                while (!command.IsCompleted)
                {
                    unityContext.Post(_=>EditorUtility.DisplayProgressBar("ADB", "Adb Process Was Not Launched. Launching new Daemon", (float) DateTime.Now.Subtract(commandStartTime).TotalMilliseconds / timeout),null);
                    await Task.Delay(1000 / 60);
                }

                EditorUtility.ClearProgressBar();
            }
        }

        private static bool EnsureApplicationCanRun()
        {
            var result=ReadAdbOutput($"-s {AdbDiscovery.SerialNumberOfFirstAttachedDevice()} shell pm list packages {Application.identifier}");
            //Any Kind of Adb Error
            if (result.Error.Count > 0)
            {
                for (var i = 0; i < result.Error.Count; i++)
                {
                    Debug.LogError(result.Error[i]);
                }

                return false;
            }
            
            if (result.Output.Any(package => package.Contains(Application.identifier)))  return true; //Our Application Found
          
            
            //Packages Result in Empty list if package name not found
            Debug.LogWarning("Application not installed on phone");
            return false;
        }

        private static async Task <bool> AssertAdbIsReady(bool displayErrorInEditorDialog = true)
        {
            await AssertAdbExist();

            var result = IsAnyDeviceConnected;
            if (!IsAnyDeviceConnected && displayErrorInEditorDialog)
            {
                EditorUtility.DisplayDialog("ADB", "No Device Connected To Adb", "Ok");
            }

            return result;
        }

        private static bool IsAnyDeviceConnected => GetConnectedDevices().Count > 0;
        private static bool IsInTCPIPMode
        {
            get
            {
                var connectedDevices = GetConnectedDevices();
                return connectedDevices.Count == 1 && connectedDevices[0].Length != 8;
            }
        }

        static class AdbDiscovery
        {
            public static async void Start()
            {
                if (!NetworkInterface.GetIsNetworkAvailable())
                {
                    EditorUtility.DisplayDialog("Sorry", "No Network , check your connection", "OK");
                    return;
                }

                await RestartToTcpIpMode();

                if ( GetConnectedDevices().Count<1 || await TryFindIpByUsbDeviceAdbAndConnect()=="IPParseError")
                {
                    await ScanAndConnectToFirstAvailableDevice();
                }
            }

            public static async Task<string> TryFindIpByUsbDeviceAdbAndConnect()
            {
                var allAdbLines = ReadAdbOutputLines("-d shell \"ip route | awk '{print $9}'\"");
                var ipFromAdb   =allAdbLines.FirstOrDefault();
             
                if (!string.IsNullOrEmpty(ipFromAdb))
                {
                    var foundIp = Regex.Match(ipFromAdb, @"((?:\d+\.){3}\d+)");

                    if (foundIp.Success)
                    {
                        var ip = foundIp.Captures.FirstOrDefault().Value;

                        var pcIpDomain = GetIPDomain;
                        
                        if (!ip.Contains(pcIpDomain))
                        {
                            EditorUtility.DisplayDialog("ADB",$"Your Computer Domain [{pcIpDomain}] and Mobile [{ip}] are on different networks!!!","Ok");
                            return "DifferentNetworks";
                        }
                       
                        RunAdbCommand($"connect {ip}", sendToFirstAvailableDevice:false).WaitForExit();
                        var modelName = ReadAdbOutputLines($"-s {ip} shell getprop ro.oppo.market.name").FirstOrDefault() ?? "Unrecognized"; // Device Model Name 
                        EditorUtility.DisplayDialog("ADB", $"Connected to [{modelName.ToUpper()}] on \n{ip}:5555\nDisconnect USB !!!", "ok");
                        
                        return "Success";
                    }
                    
                    return "IPParseError";
                }

                EditorUtility.DisplayDialog("ADB", $"Your Mobile Is Not Connected To Network", "Ok");
                return "WifiOff";
            }
            public static async Task ScanAndConnectToFirstAvailableDevice()
            {
                var unityContext = SynchronizationContext.Current;

                EditorUtility.DisplayProgressBar("ADB", "Scanning IPS", 0.25f);
                
                try
                {
                    int                     counter     = 0;
                    bool                    found       = false;
                    
                    CancellationTokenSource cancelTasks = new CancellationTokenSource();
                    var                     ipDomain    = GetIPDomain;

                    var scanStartIp = 1;
                    var scanEndIp   = 255;
                    var tasks       = new Task[scanEndIp - scanStartIp + 1];

                    for (int i = scanStartIp; i <= scanEndIp; i++)
                    {
                        var threadSafeIndex = i;
                        tasks[i - scanStartIp] = Task.Run(() =>
                        {
                            if (cancelTasks.IsCancellationRequested) return;
                            using var ping  = new Ping();
                            var       reply = ping.Send($"{ipDomain}{threadSafeIndex}", 200);
                            if (cancelTasks.IsCancellationRequested) return;
                            counter++;

                            //Sending To Unity Main GUI Thread-----------------
                            unityContext.Post(_ =>
                            {
                                if (!cancelTasks.IsCancellationRequested && !found)
                                {
                                    if (EditorUtility.DisplayCancelableProgressBar("ADB", $"Scanning IPS [{counter} /{tasks.Length}]", 0.25f + 0.75f * counter / 253f)) //If True - User Clicked Cancel
                                    {
                                        cancelTasks.Cancel();
                                        EditorUtility.ClearProgressBar();
                                    }
                                }
                            }, null);
                            //----------------------------------------------------

                            if (cancelTasks.IsCancellationRequested) return;
                            if (reply?.Status == IPStatus.Success)
                            {
                                //Todo Refactor This
                                try
                                {
                                    using var tcpClient= new TcpClient(reply.Address.ToString(), 5555); // Try to Connect Any Device where port 5555 is open

                                    RunAdbCommand($"connect {reply.Address}", sendToFirstAvailableDevice:false).WaitForExit();   // Try Connect Adb to Device
                                    var adbOutput=ReadAdbOutputLines($"-s {reply.Address} shell getprop ro.serialno"); //Try to Get Serial Number from Adb Shell

                                    foreach (var line in adbOutput)
                                    {
                                        if (line.Contains("offline",      StringComparison.InvariantCultureIgnoreCase) //Or- Adb is NOT connected to mobile device but something else (like router with open port)
                                            || line.Contains("error",     StringComparison.InvariantCultureIgnoreCase)
                                            || line.Contains("not found", StringComparison.InvariantCultureIgnoreCase)) //--------------------------------               
                                        {
                                            RunAdbCommand($"disconnect {reply.Address}", logDataInConsole: false, logErrorsInConsole: false, sendToFirstAvailableDevice: false); //Disconnect from this device
                                            break;
                                        }

                                        //Otherwise if we found exact desired device , or we accept any adb device , notify user we have connected to it
                                        var modelName = ReadAdbOutputLines($"-s {reply.Address} shell getprop ro.oppo.market.name").FirstOrDefault() ?? "Unrecognized"; // Device Model Name 
                                        unityContext.Post(_ => EditorUtility.ClearProgressBar(), null);
                                        found = true;
                                        cancelTasks.Cancel();
                                        unityContext.Post(_ => EditorUtility.DisplayDialog("ADB", $"Connected to [{modelName.ToUpper()}] on \n{reply.Address}:5555\nDisconnect USB !!!", "ok"), null);
                                    }

                                }
                                catch { }
                            }
                        }, cancelTasks.Token);
                    }

                    await Task.Run(() =>
                    {
                        try {Task.WaitAll(tasks);} 
                        catch{} // suppress task cancellation exception
                        unityContext.Post(_ => EditorUtility.ClearProgressBar(), null);
                        if (!found)
                        {
                            unityContext.Post(_ => EditorUtility.DisplayDialog("ADB", $"Can't Find Device", "ok"), null);
                        }
                    });
                }

                finally
                {
                    EditorUtility.ClearProgressBar();
                }
            }
            

            private static async Task RestartToTcpIpMode()
            {
                //Restart In TcpIp Mode--------------------------------
                RunAdbCommand("tcpip 5555", sendToFirstAvailableDevice: false); //This Function will return immediately even with WaitForExit. So Wait Manually 
               
                //----------------------- Animate Progress Bar While Waiting
                
                var waitedMilliseconds            = 0; 
                var waitForAdbInMilliseconds      = 3000f;
                var animateIntervalInMilliseconds = 100;
                while (waitedMilliseconds < waitForAdbInMilliseconds) // Progress Bar Animation
                {
                    EditorUtility.DisplayProgressBar("ADB", $"Setting TcpIp Mode For Adb ... wait {waitForAdbInMilliseconds / 1000} seconds", waitedMilliseconds / waitForAdbInMilliseconds);
                    await Task.Delay(animateIntervalInMilliseconds);
                    waitedMilliseconds += animateIntervalInMilliseconds;
                }
                
                //--------------------------------------------------
            }

            /// <summary>
            /// Serial Number (As Shown in Adb list ) of First Attached Device / Or Null 
            /// </summary>
            /// <returns></returns>
            public static string SerialNumberOfFirstAttachedDevice() => GetConnectedDevices().FirstOrDefault();
           
            /// <summary>
            /// Unique Physical Serial Number (Written in Device) of First Attached Device / Or Null 
            /// </summary>
            /// <returns></returns>
            public static string PhysicalSerialNumberOfFirstAttachedDevice()
            {
                var firstAvailableDevice = SerialNumberOfFirstAttachedDevice();
                if (firstAvailableDevice == default) return null;
                return ReadAdbOutputLines($"-s {firstAvailableDevice} shell getprop ro.serialno").FirstOrDefault();
            }
            
            private static string GetIPDomain
            {
                get
                {
                    try
                    {
                        using Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0);
                        socket.Connect("8.8.8.8", 65530);
                        IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;

                        var localIP = endPoint.Address.ToString();
                        localIP = localIP.Substring(0, localIP.LastIndexOf(".") + 1);
                        return localIP;
                    }

                    catch (Exception e)
                    {
                        Debug.LogError("Seems There is Network Connection Error" + e.Message);
                        throw;
                    }
                }
            }
        }
    }
}

#endif