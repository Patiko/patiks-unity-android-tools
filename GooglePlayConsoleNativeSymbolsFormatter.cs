﻿#if UNITY_EDITOR
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Patik.EditorTools.Android.Shared;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine.UDP;
using Debug = UnityEngine.Debug;

namespace Patik.EditorTools.Android
{
    public static class GooglePlayConsoleNativeSymbolsFormatter
    {
        private const string ARM64FolderSignature = "arm64-v8a";
        private const string Arm7FolderSignature  = "armeabi-v7a";
        
        [MenuItem("Tools/Android/GooglePlayConsole/FormatSymbols")]
        public static async void FormatSymbols()
        {
            var editorRootFolder = Path.GetDirectoryName(EditorApplication.applicationPath);
            var zipTool = Path.Combine(editorRootFolder,"Data/Tools/7z.exe");
      

            //Get Unity Platform Installation Path from Unity Editor Folder (PlaybackEngines/Android)
            var UnityAndroidFolder = Path.Combine(editorRootFolder, "Data/PlaybackEngines/AndroidPlayer/");
            if (DoesNotExist(UnityAndroidFolder, "Android Platform installation was not found for Unity Editor")) return;

            //Get NDK Folder
            var NDKToolChainsFolder = Path.Combine(UnityAndroidFolder, "NDK/toolchains");
            if(DoesNotExist(NDKToolChainsFolder, $"NDK not installed as part of Unity , Folder {NDKToolChainsFolder} does not exist")) return;
            
            //*.So Strippers for Arm7 and Arm64
            var arm7StripperToolPath = Path.Combine(NDKToolChainsFolder, @"arm-linux-androideabi-4.9/prebuilt/windows-x86_64/bin/arm-linux-androideabi-objcopy.exe");
            var arm64StripperToolPath = Path.Combine(NDKToolChainsFolder, @"aarch64-linux-android-4.9/prebuilt/windows-x86_64/bin/aarch64-linux-android-objcopy.exe");
            
            if(DoesNotExist(arm7StripperToolPath, $"Can't Find File {arm7StripperToolPath}")) return;
            if(DoesNotExist(arm64StripperToolPath, $"Can't Find File {arm64StripperToolPath}")) return;

            var ill2CppUnitySymMainFolder = Path.Combine(UnityAndroidFolder, @"Variations\il2cpp\Release\Symbols");
            
            var symbolsZip= EditorUtility.OpenFilePanel("Symbols Unzipped Folder", "Symbols Unzipped Folder", "");
            if(string.IsNullOrEmpty(symbolsZip)) return;
            
            var unzippedOriginalSimsFolder = Path.Combine(Path.GetDirectoryName(symbolsZip), "StrippingTempFiles");
          
            //We Dont need these files
            var exclusionCommand = new StringBuilder();
            var excluded         = new[]{"libil2cpp.so.debug", "libil2cpp.dbg.so"};
            excluded.ForEach(fileName => exclusionCommand.Append($" -xr!{fileName}"));
            //-----------
            
            EditorUtility.DisplayProgressBar("Extracting","Extracting Original Symbols.zip ", 0f);
            var extractingSuccess = true;
            SynchronizationContext editorContext= SynchronizationContext.Current;
            
            var lastParsedProgress = 0;
            await ProcessCommunicator.RunCommandAsync(zipTool, $"-bsp1 x \"{symbolsZip}\" -o\"{unzippedOriginalSimsFolder}\" -aoa{exclusionCommand}",
                                           onErrorOutput: (_, output) =>
                                           {
                                               if(string.IsNullOrEmpty(output.Data))return;
                                               extractingSuccess = false;
                                               editorContext.Post(O=>EditorUtility.ClearProgressBar(),null);
                                               editorContext.Post(O=>EditorUtility.DisplayDialog("Extracting Failed ", "Extracting Failed , See Console For Errors", "OK"),null);
                                           }, onDataOutput: (O,output) =>
                                           {
                                               if(string.IsNullOrEmpty(output.Data)) return;
                                               var progressStringMatch=Regex.Match(output.Data, @"\d+\%");
                                               
                                               if (progressStringMatch.Success)
                                               {
                                                   lastParsedProgress = int.Parse(progressStringMatch.Value.Trim('%'));
                                                   editorContext.Post(_=>EditorUtility.DisplayProgressBar("Extracting", $"[{lastParsedProgress}%]".PadRight(100-lastParsedProgress,'-'), 0.01f *lastParsedProgress), null);
                                               }
                                           });
            
            EditorUtility.ClearProgressBar();
            
            if(!extractingSuccess) return;
                
            //Enlist Sub folders folder like "arm64-v8a" or "armeabi-v7a" in Original Symbols Directory
            foreach (var unzippedSubFolder in Directory.GetDirectories(unzippedOriginalSimsFolder))
            {
                //And Create Same Folders in new Output folder
                var armSpecificOutputDirectoryPath = unzippedSubFolder;
                if (!Directory.Exists(armSpecificOutputDirectoryPath))
                    Directory.CreateDirectory(armSpecificOutputDirectoryPath);

                //Pure Directory Name of Folder without Whole Path ("arm64-v8a" or "armeabi-v7a")
                var outputDirectoryName = new DirectoryInfo(armSpecificOutputDirectoryPath).Name;

                //Select Correct Stripper Tool using Folder Name
                var stripperToolPath = string.Empty;
                switch (outputDirectoryName)
                {
                    case Arm7FolderSignature:
                        stripperToolPath = arm7StripperToolPath;
                        break;
                    case ARM64FolderSignature:
                        stripperToolPath = arm64StripperToolPath;
                        break;
                    default: throw new Exception("Dont Know Which tool to use for folder :" + outputDirectoryName);
                }



                //Strip File and Overwrite
                var filesToStrip = Directory.GetFiles(unzippedSubFolder).ToList();
                var progress     = 0f;

                foreach (var file in filesToStrip)
                {
                    EditorUtility.DisplayProgressBar("Stripping Build Symbol Files", Path.GetFileName(file), ++progress / filesToStrip.Count);
                    ProcessCommunicator.RunCommand(stripperToolPath, $"--strip-debug \"{file}\"");
                }


                //Grab Unity LibUnity and LibMain Files From Editor Folder
                foreach (var unitySimbolsDirectory in Directory.GetDirectories(ill2CppUnitySymMainFolder).Where(x => x.Contains(outputDirectoryName)))
                {
                    var filesToCopy = Directory.GetFiles(unitySimbolsDirectory);
                    progress = 0f;

                    foreach (var file in filesToCopy)
                    {
                        var newPath = Path.Combine(armSpecificOutputDirectoryPath, Path.GetFileName(file));
                        EditorUtility.DisplayProgressBar("Copying Unity Library Symbos", Path.GetFileName(file), ++progress / filesToCopy.Length);
                        File.Copy(file, newPath, true);
                    }

                }
            }

            //Fix Names So Play Console Take Them
                foreach (var file in Directory.GetFiles(unzippedOriginalSimsFolder,"*",SearchOption.AllDirectories))
                {
                    var newName = $"{file.Split('.')[0]}.so.sym";
                    if (File.Exists(newName))
                    {
                        File.Delete(newName);
                    }
                    File.Move(file,newName);
                }
                
                //Zip Back
                var newZipName = symbolsZip.Replace(Path.GetFileName(symbolsZip), "stripped-" + Path.GetFileName(symbolsZip));
                
                await ProcessCommunicator.RunCommandAsync(zipTool, $"-bsp1 a \"{newZipName}\" \"{unzippedOriginalSimsFolder}\"\\* -sdel",
                                                          onErrorOutput: (_, output) =>
                                                          {
                                                              if(string.IsNullOrEmpty(output.Data))return;
                                                              extractingSuccess = false;
                                                              editorContext.Post(O=>EditorUtility.ClearProgressBar(),                                                                      null);
                                                              editorContext.Post(O=>EditorUtility.DisplayDialog("Compression Failed ", "Compression Failed , See Console For Errors", "OK"), null);
                                                          }, onDataOutput: (O, output) =>
                                                          {
                                                              if(string.IsNullOrEmpty(output.Data)) return;
                                                              var progressStringMatch =Regex.Match(output.Data, @"\d+\%");
                                               
                                                              if (progressStringMatch.Success)
                                                              {
                                                                  lastParsedProgress = int.Parse(progressStringMatch.Value.Trim('%'));
                                                                  editorContext.Post(_=>EditorUtility.DisplayProgressBar("Compressing Files", $"[{lastParsedProgress}%]".PadRight(100 -lastParsedProgress, '-'), 0.01f *lastParsedProgress), null);
                                                              }
                                                          });


                if (Directory.Exists(unzippedOriginalSimsFolder))
                {
                    Directory.Delete(unzippedOriginalSimsFolder);
                }
            
            EditorUtility.ClearProgressBar();
        }

      

        private static bool DoesNotExist(string path,string errorMessage)
        {
            var assertionFailed = !(Directory.Exists(path) || File.Exists(path)); 
            if (assertionFailed)
            {
                Debug.Log(path);
                EditorUtility.DisplayDialog("Play Console Symbol Formatter", errorMessage, "Ok");
            }
            return assertionFailed;
        }
    }
}

#endif
