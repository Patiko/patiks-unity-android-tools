﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Debug = UnityEngine.Debug;

namespace Patik.EditorTools.Android.Shared
{
    public static class ProcessCommunicator
    {
        public static Task RunCommandAsync(string                   processPath,
                                           string                   command,
                                           bool                     logDataInConsole    = false,
                                           bool                     logErrorsInConsole    = true,
                                           DataReceivedEventHandler onDataOutput          = null,
                                           DataReceivedEventHandler onErrorOutput         = null,
                                           EventHandler             onExit                = null,
                                           int                      timeoutInMilliseconds = -1)
        {
            var process = CreateProcess(processPath, command, logDataInConsole, logErrorsInConsole,onDataOutput, onErrorOutput, onExit);
            //Run Process Awaiter onExternal Thread so it won't block main thread
            return Task.Run(() =>
            {
                if (timeoutInMilliseconds > 0)
                {
                    if (!process.WaitForExit(timeoutInMilliseconds))
                        process.Kill();
                }
                else
                {
                    process.WaitForExit();
                }
            });
        }

        public static Process RunCommand(string                   processPath, string command,
                                         bool                     logDataInConsole   = false,
                                         bool                     logErrorsInConsole = true,
                                         DataReceivedEventHandler onDataOutput       = null,
                                         DataReceivedEventHandler onErrorOutput      = null,
                                         EventHandler             onExit             = null)
        {
            var process = CreateProcess(processPath, command, logDataInConsole,logErrorsInConsole, onDataOutput, onErrorOutput, onExit);
            process.WaitForExit();
            return process;
        }

        public static Process CreateProcess(string                   processPath, string command,
                                            bool                     logDataInConsole  = false,
                                            bool                     logErrorsInConsole  = true,
                                            DataReceivedEventHandler onDataOutput  = null,
                                            DataReceivedEventHandler onErrorOutput = null,
                                            EventHandler             onExit        = null)
        {
            var settings                                                     = new ProcessStartInfo(processPath, command);
            settings.RedirectStandardError = settings.RedirectStandardOutput = settings.CreateNoWindow = true;
            settings.UseShellExecute       = false;
            var process = Process.Start(settings);
            process.EnableRaisingEvents = true;


            if (onDataOutput  != null) process.OutputDataReceived += onDataOutput;
            if (onErrorOutput != null) process.ErrorDataReceived  += onErrorOutput;
            if (onExit        != null) process.Exited             += onExit;


            if (logDataInConsole)
            {
                process.OutputDataReceived += (sender, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data)) Debug.Log($"[{process.ProcessName}] :{args.Data}");
                };
            }

            if (logErrorsInConsole)
            {
                process.ErrorDataReceived += (sender, args) =>
                {
                    if (!string.IsNullOrEmpty(args.Data)) Debug.LogError($"[{process.ProcessName}] :{args.Data} -> [Command: {command}]");
                };
            }

            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            return process;
        }

        public static List <string> ReadProcessOutputLines(string processPath, string command)
        {
            List <string> text=new List <string>();
            
            void OutPutHandler(object sender, DataReceivedEventArgs args)
            {
                if (!string.IsNullOrEmpty(args.Data))
                {
                    text.Add(args.Data);
                }
            }
            RunCommand(processPath, command, false, false, OutPutHandler, OutPutHandler);
            return text;
        }
        
        public static (List<string> Output , List<string> Error) ReadProcessOutput(string processPath, string command)
        {
            List<string> Response = new List<string>();
            List<string> Error= new List<string>();

            void OutPutHandler(object sender, DataReceivedEventArgs args)
            {
                if (!string.IsNullOrEmpty(args.Data))
                {
                    Response.Add(args.Data);
                }
            }
            void ErrorHandler(object sender, DataReceivedEventArgs args)
            {
                if (!string.IsNullOrEmpty(args.Data))
                {
                    Error.Add(args.Data);
                }
            }

            RunCommand(processPath, command, false, true,OutPutHandler, ErrorHandler);
            return (Response,Error);
        }
    }
}
#endif